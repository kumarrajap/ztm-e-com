import './App.css';

import HomePage from './Homepage.component';

const App = () => {
  return (
    <div >
      <HomePage />
    </div>
  );
}

export default App;
